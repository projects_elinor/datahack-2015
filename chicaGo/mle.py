__author__ = 'Elinor'
import numpy as np
import pandas as pd
from pandas import DataFrame, Series
import statsmodels.formula.api as sm
from sklearn.linear_model import LinearRegression
import scipy, scipy.stats
import matplotlib.pyplot as plt
from sklearn import datasets, linear_model


def last_days_bernuly(iblock, idate, num_of_days=60):
    df = pd.read_csv("C:\Users\Elinor Brondwine\Desktop\RawData_ChicaGO\ChicaGo_2015_Elinor_with_fetures.csv",
                     parse_dates={"newDateOnly": ['Date']})
    df = df[df["Block"] == iblock]
    print df
    df['OnlyDate'] = pd.to_datetime(df.OnlyDate)

    lastdayfrom = pd.to_datetime(idate)
    df = df.reset_index().set_index('OnlyDate')
    df.index = df.index.sort_values()
    df = df.loc[lastdayfrom - pd.Timedelta(days=num_of_days):lastdayfrom].reset_index()
    # print [col for col in df.columns if 'BinHours' in str(col)]
    index_list = [col for col in df.columns if 'BinHours' in str(col)]
    index_list += ['OnlyDate']
    positives = df.groupby(index_list).count()
    intervals = float(num_of_days) * 8
    probability = float(positives.shape[0]) / intervals

    return probability


csvfile = "C:\Users\Elinor Brondwine\Desktop\RawData_ChicaGO\ChicaGo_RowData_2011_2015_with_fetures_Elinor.csv"


def data_frame_simple_bernuli(csvfile):
    df = pd.read_csv(csvfile, parse_dates={"newDateOnly": ['Date']})
    df['OnlyDate'] = pd.to_datetime(df.OnlyDate)
    num_of_days = float(len(set(df['OnlyDate'])))
    # df=df.reset_index().set_index('OnlyDate')
    # df.index=df.index.sort_values()
    # print [col for col in df.columns if 'BinHours' in str(col)]
    index_list_h = [col for col in df.columns if 'BinHours_' in str(col)]
    index_list_d = [col for col in df.columns if 'DayOfWeek_' in str(col)]
    index_list_m = [col for col in df.columns if 'Month_' in str(col)]
    index_list = index_list_h + index_list_d + index_list_m + ['Block']
    positives = df.groupby(index_list).count()
    print positives


import json

file = open("C:\Users\Elinor Brondwine\Desktop\RawData_ChicaGO\\route_w_crimes_f.json", "r")
pathsj = json.load(file)
i = 0
paths = {}
for comp in pathsj:
    paths[i] = comp["crimes"]
    i += 1

print paths

idate = "11/26/15"
itime = "12:30"


def cvm_attampt(csvfile, paths, idate, itime):
    table = pd.read_csv(csvfile, parse_dates={"newDateOnly": ['Date']})
    df = table
    df['Root'] = "else"
    for pathID in paths.keys():
        for block in paths[pathID]:
            df['Root'][df.Block == block] = "Path_Block_" + str(pathID)
    df = df[df['Root'] != "else"]
    df['OnlyDate'] = pd.to_datetime(df.OnlyDate)
    num_of_days = float(len(set(df['OnlyDate'])))
    df = df.reset_index().set_index('OnlyDate')
    df.index = df.index.sort_values()
    measures = pd.DataFrame(df.groupby('Root').count()['ID'])
    measures.columns = ['all']
    print measures

    ###now we're talking.
    ##week of the year:
    # df_month_only
    lastdayfrom = pd.to_datetime(idate)
    dlastmonth = df.loc[lastdayfrom - pd.Timedelta(days=30):lastdayfrom].reset_index()
    # print [col for col in df.columns if 'BinHours' in str(col)]
    d = pd.DataFrame(dlastmonth.groupby('Root').count()['ID'])
    d.columns = ['last_month_data']
    measures["last_month_data"] = d
    print measures
    # df_each_month
    day_of_week = \
    [u'DayOfWeek_Mon', u'DayOfWeek_Tue', u'DayOfWeek_Wed', u'DayOfWeek_Thu', u'DayOfWeek_Fri', u'DayOfWeek_Sat',
     u'DayOfWeek_Sun'][lastdayfrom.dayofweek]
    dataforallmondays = df[df[day_of_week] == 1]
    d = pd.DataFrame(dataforallmondays.groupby('Root').count()["ID"])
    d.columns = ['last_month_data']
    measures["same_day_all_years"] = d['last_month_data']
    print measures
    districscore = {20: 1, 24: 2, 22: 2, 17: 2, 16: 3, 15: 4, 1: 5, 10: 5, 19: 5, 2: 5, 3: 5, 5: 5, 9: 5, 12: 5, 13: 5,
                    14: 5, 18: 5, 21: 5, 23: 5, 31: 5, 4: 6, 6: 6, 7: 7, 25: 7, 8: 8, 11: 8}
    df['district_score'] = df['District'].apply(lambda x: districscore[int(x)])
    df['district_score'] = df['district_score']
    # print df['district_score']
    # print df[['district_score','Root']].groupby( 'Root' ).sum( )
    d = pd.DataFrame(df.groupby('Root').sum()['district_score'])
    measures['district_score'] = d['district_score']
    # print measures
    currentHour = itime.split(':')[0]
    binhour = ''
    currentHour = int(currentHour)
    if (currentHour >= 0) and (currentHour <= 2):
        binhour = 'BinHours_0-3'
    elif (currentHour >= 3) and (currentHour <= 5):
        binhour = 'BinHours_3-6'
    elif (currentHour >= 6) and (currentHour <= 8):
        binhour = 'BinHours_6-9'
    elif (currentHour >= 9) and (currentHour <= 11):
        binhour = 'BinHours_9-12'
    elif (currentHour >= 12) and (currentHour <= 14):
        binhour = 'BinHours_12-15'
    elif (currentHour >= 15) and (currentHour <= 17):
        binhour = 'BinHours_15-18'
    elif (currentHour >= 18) and (currentHour <= 20):
        binhour = 'BinHours_18-21'
    elif (currentHour >= 21) and (currentHour <= 23):
        binhour = 'BinHours_21-24'
    hdataforallmondays = dataforallmondays[dataforallmondays[binhour] == 1]
    d = pd.DataFrame(hdataforallmondays.groupby('Root').count()["ID"])
    d.columns = ['all_monday_h_data']
    measures["all_monday_h_data"] = d['all_monday_h_data']
    weeks_legend = pd.DataFrame(df.groupby('WeekOfYear').count()["ID"])
    # print weeks_legend
    weeks = df[df['WeekOfYear'] == lastdayfrom.weekofyear]
    d = pd.DataFrame(weeks.groupby('Root').count()["ID"])
    d.columns = ['weeks']
    measures["weeks"] = d['weeks']
    print measures
    # print measures.apply(np.sum)
    return measures.apply(np.sum, axis=1)


"""



	index_list=[col for col in df.columns if 'BinHours' in str( col )]
	index_list+=['OnlyDate']
	positives=df.groupby(index_list).count( )
	intervals=float(num_of_days)*8
	# df=df.reset_index().set_index('OnlyDate')
	#df.index=df.index.sort_values()
	#print [col for col in df.columns if 'BinHours' in str(col)]
	#index_list_h=[col for col in df.columns if 'BinHours_' in str( col )]
	#index_list_d=[col for col in df.columns if 'DayOfWeek_' in str( col )]
	#index_list_m=[col for col in df.columns if 'Month_' in str( col )]
	#index_list=index_list_h+index_list_d+index_list_m+['Block']
	#positives=df.groupby( index_list ).count( )
	print positives
"""

# intervals=float(num_of_days)*8
# positives['probability']=float( positives.shape[0] )/intervals
# intervals=float(num_of_days)*8
# positives['probability']=float( positives.shape[0] )/intervals

cvm_attampt(csvfile, paths, idate, itime)
"""
def data_frame_simple_bernuli(csvfile):
	df=pd.read_csv( csvfile ,parse_dates={"newDateOnly" : ['Date']})
	df['OnlyDate']=pd.to_datetime(df.OnlyDate)
	num_of_days=float(len(set(df['OnlyDate'])))
	#df=df.reset_index().set_index('OnlyDate')
	#df.index=df.index.sort_values()
	#print [col for col in df.columns if 'BinHours' in str(col)]
	index_list_h=[col for col in df.columns if 'BinHours_' in str(col)]
	index_list_d=[col for col in df.columns if 'DayOfWeek_' in str(col)]
	index_list_m=[col for col in df.columns if 'Month_' in str(col)]
	index_list=index_list_h+index_list_d+index_list_m+['Block']
	positives= df.groupby(index_list).count()
	print positives
	#intervals=float(num_of_days)*8
	#positives['probability']=float( positives.shape[0] )/intervals
"""

# final=pd.pivot_table(df,index=index_list,values='ID',columns=compare, aggfunc=np.mean)
# print final
# print df[cols]
# print df.groupby( df[cols] )



# print df
"""


df.head(5)


df['date_no_hour']=df['Date'].apply( lambda x: x.split()[0] )
df['Date_District']=df['date_no_hour'].astype('str')+'_'+df['District'].astype('str')
df['Date_Block']=df['date_no_hour'].astype('str')+'_'+df['District'].astype('str')
#df['date_no_hour']
new=pd.DataFrame(df.groupby( df['Date_District'] ).count()['Date']).astype('float')
new.columns=['ncount']
new=new.reset_index()
new['date_no_hour']=df['Date_District'].apply( lambda x: x.split('_')[0] )
new['dicst']=df['Date_District'].apply(lambda x: x.split('_')[1])
print new['date_no_hour'].unique.head(7)
#print pd.get_dummies(new, columns=['date_no_hour'])
new['lastdays'] = np.ones(( len(new['date_no_hour']), ))
train=new.head(8).tail(7)
test=new.head(1)
regr = linear_model.LinearRegression()
regr.fit(train['date_no_hour'], train['ncount'])
#The coefficients
print('Coefficients: \n', regr.coef_)
# The mean square error

#
#print df.head(1000)
"""
"""
df['month']=df['Datetime'].astype(str).apply( lambda x: x.split('-')[1] )
df['day_at_month']=df['Datetime'].astype(str).apply( lambda x: x.split('-')[2].split()[0] )
df['time_period']=df['Datetime'].astype(str).apply( lambda x: x.split()[1].split(':')[0] )
"""
