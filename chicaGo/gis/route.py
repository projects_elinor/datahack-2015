import os
import shapely
from shapely.geometry import LineString, Point
import directions
import pandas as pd
from geopy import distance
from rtree import index

google = directions.Google()
mq = directions.Mapquest('MYNjDBBCsK5j1fS0C65imHVFGK3tYAWn')
distance.VincentyDistance.ELLIPSOID = 'WGS-84'
dist_calculator = distance.distance

idx = index.Index()

base_path = os.path.dirname(__file__)
df = pd.read_csv(os.path.join(base_path, 'BlocksLatLong.txt'), sep='\t')
df['id'] = df.index

# index all blocks
df.apply(lambda x: idx.insert(x.id, (x['long mean'], x['lat mean'], x['long mean'], x['lat mean']),
                              {"block": x['Block'], "point": Point(x['long mean'], x['lat mean'])}), axis=1)


def get_crimes_around_route(route, max_dist):
    # get all blocks in the bounding box of the route
    line = LineString(route.__geo_interface__["geometry"]["coordinates"])
    bbox = line.bounds
    bbox_box = shapely.geometry.box(bbox[0], bbox[1], bbox[2], bbox[3])
    bbox_enlarge = bbox_box.buffer(0.004)
    bbox = bbox_enlarge.bounds
    close_blocks = []
    crimes_in_bbox = [n.object for n in idx.intersection(bbox, objects=True)]
    for obj in crimes_in_bbox:
        dist = get_distance_route_point(route, obj["point"])
        obj["point"] = obj["point"].xy
        if dist < max_dist:
            close_blocks.append(obj)
    return close_blocks


def get_routes(x, y):
    """
    :param x:
    :param y:
    :return: list of lists?
    """
    routes = mq.route(x, y)
    routes2 = google.route(x, y, alternatives="true", mode="walking")
    return routes + routes2


def get_distance_line_point(line, point):
    closest_point_on_line = line.interpolate(line.project(point))
    dist = dist_calculator((closest_point_on_line.xy[0][0], closest_point_on_line.xy[1][0]),
                           (point.xy[0][0], point.xy[1][0]))
    return dist.meters


def get_distance_route_point(route, point):
    line = LineString(route.__geo_interface__["geometry"]["coordinates"])
    return get_distance_line_point(line, point)
