from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

import client.views

urlpatterns = patterns('',
                       url(r'^$', client.views.index, name='index'),
                       url(r'^db', client.views.db, name='db'),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^client/routes/$', client.views.RoutesApi.as_view())

                       )
