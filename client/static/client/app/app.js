/**
 * Created on 11/26/15.
 */

var app = angular.module('chicago', [
    'uiGmapgoogle-maps',
    'directives',
    'ngMaterial',
    'ngRoute',
    'api',
    'constants'
]);

angular.module('directives', []);

app.config([
    '$routeProvider', '$sceDelegateProvider', 'STATIC',
    function ($routeProvider, $sceDelegateProvider, STATIC) {

        $sceDelegateProvider.resourceUrlWhitelist([
            'self',
            STATIC('**')
        ]);

        $routeProvider.when('/', {
            templateUrl: STATIC('app/index.html'),
            controller: 'BaseCtrl'
        });
    }
]);

app.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyCvxVX1PCU3CN23NqmQx76WD3cr5ZzSaWY',
        v: '3.20', //defaults to latest 3.X anyhow
        libraries: 'weather,geometry,visualization'
    });
});

app.constant('lodash', window._);
