/**
 * Created on 11/26/15.
 */


var module = angular.module('api', []);

module.factory('RoutesAPI', ['lodash', '$http', '$q', function(_, $http, $q) {
    this.getRoutes = function (startMarker, endMarker) {
        return $http.get(_.template('/client/routes/?start=${start}&end=${end}')({
            start: _.map(startMarker.position, function (val) { return val; }),
            end: _.map(endMarker.position, function (v) { return v;})
        }))
    };

    return this;
}]);
